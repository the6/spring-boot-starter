@Configuration 
@EnableConfigurationProperties(EventListenerProperties.class)
@ConditionalOnProperty(value = "eventstarter.enabled", havingValue = "true")
@ConditionalOnClass(name = "io.reflectoring.KafkaConnector")
class EventAutoConfiguration {

	@Bean
	EventPublisher eventPublisher(List<EventListener> listeners){
		return new EventPublisher(listeners);
	}
}