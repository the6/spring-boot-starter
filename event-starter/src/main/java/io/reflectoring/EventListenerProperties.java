@ConfigurationProperties(prefix = "eventstarter.listener")
@Data
class EventListenerProperties {

	/**
	* List of event types that will be passed to {@link EventListener}
	* implementations. All other events will be ignored 
	*/
	private List<String> enabledEvents = Collections.emptyList();
}